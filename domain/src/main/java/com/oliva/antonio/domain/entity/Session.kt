package com.oliva.antonio.domain.entity

/**
 * Created by antonio on 1/21/18.
 */

data class Session(val name: String,
                   val champName: String,
                   val startTime: String,
                   val endTime: String)