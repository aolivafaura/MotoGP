package com.oliva.antonio.data.entity.session

/**
 * Created by antonio on 1/20/18.
 */

internal data class SessionEntity(val name: String,
                                  val champName: String,
                                  val startTime: String,
                                  val endTime: String)